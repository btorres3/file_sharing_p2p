import os
import sys
import time
import socket
import threading
import xmlrpc.client
from xmlrpc.client import ServerProxy
from xmlrpc.server import SimpleXMLRPCServer

client_side_port = 12345

class Peer:
    """Implements the code necessary for each Peer"""
    # File array for each peer
    files = []
    # proxy that connects to indexing server's RPC server.
    proxy = ServerProxy('http://localhost:9090')
    
    def __init__(self, peer_id, server_port, rpc_port, address):
        """Initialize the peer with user-provided id, server-side port, rpc port, and address."""
        self.peer_id = peer_id
        self.server_port = server_port
        self.rpc_port = rpc_port
        self.address = address
        # Peer will look at its directory and create the array of file names
        self._create_file_list()
        # The Peer will then send its file array to the index server
        self._send_to_server()
        # Assign one thread to run the RPC server
        threading.Thread(target=self._run, args=(rpc_port,), daemon=True).start()
        # Assign one thread to run automatic updates
        threading.Thread(target=self._update, daemon=True).start()
        # Assign one thread to run the client-side of the peer
        threading.Thread(target=self._run_client, daemon=True).start()
        # The main thread will run the server-side of the peer
        self._run_server()

    def _run_client(self):
        """Run the client-side of the peer"""
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.connect((self.address, client_side_port))
        while True:
            data = sock.recv(1024*10)
            if not data:
                break

    def _run_server(self):
        """Run the server-side of the peer"""
        server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        server_socket.bind(('', self.server_port))
        server_socket.listen(5)

        while True:
            try: 
                (client, address) = server_socket.accept()
                threading.Thread(target=self._client_handler, args=(client,address,)).start()
            except KeyboardInterrupt:
                print("Exiting...")
                sys.exit(0)     

    def _connect_to_peer(self, file_name, peer_id, client):
        """Downloads file_name from peer_id and saves it in its respective folder"""
        place_to_save = f"./resources/Peer{self.peer_id}/{file_name}"
        rpc_port = 9090 + peer_id
        proxy = ServerProxy(f'http://localhost:{rpc_port}')
        with open(place_to_save, "wb") as handle:
            handle.write(proxy.retrieve(file_name).data)
        string_to_send = f"{file_name} successfully downloaded!"
        response = bytes(string_to_send, 'utf-8')

        client.send(response)
        client.send(b'\r\n\r\n')


    def _file_search_handler(self, client):
        """Helper method that handles client's file search. Displays file and peers it belongs to."""
        while True:
            message = b'Search for a file: '
            client.send(message)
            file_name = client.recv(1024)
            decoded_file_name = file_name.decode("utf-8", "ignore").rstrip()

            proxy = ServerProxy('http://localhost:9090')
            peers = self._get_file_list(decoded_file_name)
            if peers is not None:
                for peer_id in peers:
                    if peer_id == self.peer_id:
                        string_to_send = f"I currently have this file!\r\n"
                    else:
                        string_to_send = f"Peer {peer_id} has this file.\r\n"
                    response = bytes(string_to_send, 'utf-8')
                    client.send(response)
                return (decoded_file_name, peers)

            else:
                err_msg = b'File not found! Try searching again.\r\n'
                client.send(err_msg)

    def _peer_id_connect_handler(self, peers_with_file, client):
        """Helper method that handles client's peer id selection"""
        while True:
            peer_id = 0
            message = b'What peer do you wish to download this file from? '
            client.send(message)
            str_peer_id = client.recv(8)

            try:
                peer_id = int(str_peer_id)
            except ValueError:
                err_msg1 = b'Please enter in a valid integer.\r\n'
                client.send(err_msg1)
                continue

            for peer in peers_with_file:
                if peer_id == peer:
                    return peer_id
            err_msg2 = b'Not a valid peer ID to connect to! Try searching again.\r\n'
            client.send(err_msg2)

    def _client_handler(self, client, address):
        """method that handles each client's connection"""
        client_address = address[0]
        client_port = address[1]
        while True:
            choice = self._menu(client)
            if choice == 1:
                (file_name, peers_with_file) = self._file_search_handler(client)
                peer_to_connect = self._peer_id_connect_handler(peers_with_file, client)
                self._connect_to_peer(file_name, peer_to_connect, client)
            elif choice == 2:
                self._display(client)

    def _display(self, client):
        """Helper method that displays each file and the peers it belongs to"""
        file_and_peers = self._get_files_and_peers()
        for file in file_and_peers:
            peers = file_and_peers[file]
            string_to_send = f"File: {file}, belongs to Peers: {peers}\r\n"
            msg = bytes(string_to_send, 'utf-8')
            client.send(msg)
        client.send(b'\r\n')

    def _menu(self, client):
        """Helper method that displays user-friendly menu to each client."""
        while True:
            msg1 = b'Welcome to a Peer! What would you like to do?\r\n'
            msg2 = b'----------------------------------------------------------\r\n'
            msg3 = b'1: Search for a file and download it\r\n'
            msg4 = b'2: See the list of files and the Peer(s) they belong to\r\n'
            msg5 = b'\r\n'
            msg6 = b'Choice: '
            client.send(msg1 + msg2 + msg3 + msg4 + msg2 + msg6)
            choice_string = client.recv(8)
            try:
                choice = int(choice_string)
                if choice > 2:
                    client.send(msg5)
                    err_msg = b'Please enter in a valid menu choice!\r\n'
                    client.send(err_msg)
                    client.send(msg5)
                    continue
            except ValueError:
                client.send(msg5)
                err_msg = b'Please enter in a valid integer.\r\n'
                client.send(err_msg)
                client.send(msg5)
                continue

            client.send(msg5)
            return choice  

    def _get_file_list(self, file_name):
        """Helper method that asks the server for the file list and returns it"""
        return self.proxy.rpc_search(file_name)

    def _get_files_and_peers(self):
        """Returns server's full file->peer dictionary"""
        return self.proxy.file_dict()

    def _create_file_list(self):
        """Helper method that creates the file array for each peer"""
        folder = "Peer" + str(self.peer_id)
        self.files = os.listdir(f"./resources/{folder}")

    def _send_to_server(self):
        """Helper method that sends the file array to the index server"""
        self.proxy.registry(self.peer_id, self.files)

    def retrieve(self, file_name):
        """RPC method that allows peers/clients to downloads files from this Peer instance"""
        folder = "Peer" + str(self.peer_id)
        with open(f"./resources/{folder}/{file_name}", "rb") as handle:
            return xmlrpc.client.Binary(handle.read())

    def _get_file_from_peer(self, peer_id, file_name):
        """Helper method that allows a Peer to connect to another Peer's RPC server and download files"""
        place_to_save = f"./resources/Peer{self.peer_id}/{file_name}"
        rpc_port = 9090 + peer_id
        proxy = ServerProxy(f'http://localhost:{rpc_port}')
        with open(place_to_save, "wb") as handle:
            handle.write(proxy.retrieve(file_name).data)

    def _run(self, rpc_port):
        """Helper method starts each Peer's RPC server"""
        peer_server = SimpleXMLRPCServer(('', rpc_port))
        peer_server.register_instance(self)
        peer_server.serve_forever()

    def _update(self):
        """Helper method that updates each Peer's directory every 1 second and prints deletions or additions"""
        while True:
            time.sleep(1)
            folder = "Peer" + str(self.peer_id)
            directory_to_track = os.listdir(f"./resources/{folder}")
            less = [item for item in self.files if item not in directory_to_track]
            more = [item for item in directory_to_track if item not in self.files]
            if less:
                print("File(s) deleted!")
                for item in less:
                    print(f"{item} removed.")
                    self.files.remove(item)
                    self._send_to_server()
            elif more:
                print("File(s) added!")
                for item in more:
                    print(f"{item} added.")
                    self.files.append(item)
                    self._send_to_server()

# Get the current IP address of this computer
hostname = socket.gethostname()
ip_addr = socket.gethostbyname(hostname)

# Converts command line arguments to integers and provides them to start each Peer instance
try:
    peer_id = int(sys.argv[1])
    peer_server_port = int(sys.argv[2])
    peer_rpc_port = int(sys.argv[3])
    peer = Peer(peer_id, peer_server_port, peer_rpc_port, ip_addr)
except ValueError:
    print("Please enter in a valid integer!")
    sys.exit(0)