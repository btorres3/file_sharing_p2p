import os
import sys
import socket
import threading
from xmlrpc.client import ServerProxy
from xmlrpc.server import SimpleXMLRPCServer

# Constants 
host = 'localhost'
rpc_port = 9090
socket_port = 12345
peer_files = {}
peers = []

# Create the RPC server by allowing clients to listen to the provided RPC port.
# The options allow 'None' to be marshaled and don't flood stdout with info
# on incoming RPC requests.
index_server = SimpleXMLRPCServer(
    ('', rpc_port), allow_none=True, logRequests=False)

class ServerRemoteMethods:
    """This class handles all the RPC methods the indexing server provides."""
    def registry(self, peer_id, files):
        """An RPC method that allows peers to register their files with the server."""
        for f in files:
            peer_files.setdefault(f, []).append(peer_id)
        print(f"Peer {peer_id} has connected and successfully registered its Files.")
        return 0

    def rpc_search(self, file_name):
        """Return the peers that hold file_name"""
        return search(file_name)

    def file_dict(self):
        """Return the full files to peers dictionary"""
        return peer_files

# Register an instance of ServerRemoteMethods() for it to be available to RPC requests.
index_server.register_instance(ServerRemoteMethods())

# Socket-level methods

def search(file_name):
    """Return the peers that hold file_name"""
    if file_name in peer_files:
        peer_list = peer_files[file_name]
        return peer_list
    else:
        return None


def start_remote_methods():
    """Start the RPC server to allow clients to connect"""
    try:
        index_server.serve_forever()
    except KeyboardInterrupt:
        print("Exiting...")
        sys.exit(0)


def connect_to_peer(file_name, peer_id):
    """Get file_name from peer_id and save it to the client's folder"""
    place_to_save = f"./resources/client/{file_name}"
    rpc_port = 9090 + peer_id
    proxy = ServerProxy(f'http://localhost:{rpc_port}')
    with open(place_to_save, "wb") as handle:
        handle.write(proxy.retrieve(file_name).data)

    client.send(b'\r\n\r\n')
    string_to_send = f"{file_name} successfully downloaded!"
    response = bytes(string_to_send, 'utf-8')
    client.send(response)
    client.send(b'\r\n\r\n')


def file_search_handler(client):
    """Helper function that handles client's file search. Displays file and peers it belongs to."""
    while True:
        message = b'Search for a file: '
        client.send(message)
        file_name = client.recv(1024)
        decoded_file_name = file_name.decode("utf-8", "ignore").rstrip()

        peers = search(decoded_file_name)
        if peers is not None:
            for peer_id in peers:
                string_to_send = f"Peer {peer_id} has this file.\r\n"
                response = bytes(string_to_send, 'utf-8')
                client.send(response)
            return (decoded_file_name, peers)

        else:
            err_msg = b'File not found! Try searching again.\r\n'
            client.send(err_msg)

def peer_id_connect_handler(peers_with_file, client):
    """Helper function that handles client's peer id selection"""
    while True:
        peer_id = 0
        message = b'What peer do you wish to download this file from? '
        client.send(message)
        str_peer_id = client.recv(8)

        try:
            peer_id = int(str_peer_id)
        except ValueError:
            err_msg1 = b'Please enter in a valid integer.\r\n'
            client.send(err_msg1)
            continue

        for peer in peers_with_file:
            if peer_id == peer:
                return peer_id
        err_msg2 = b'Not a valid peer ID to connect to! Try searching again.\r\n'
        client.send(err_msg2)

def client_handler(client, address):
    """Function that handles each client's connection"""
    client_address = address[0]
    client_port = address[1]
    while True:
        choice = menu(client)
        if choice == 1:
            (file_name, peers_with_file) = file_search_handler(client)
            peer_to_connect = peer_id_connect_handler(peers_with_file, client)
            connect_to_peer(file_name, peer_to_connect)
        elif choice == 2:
            display(client)

def display(client):
    """Helper function that displays each file and the peers it belongs to"""
    for file in peer_files:
        peers = peer_files[file]
        string_to_send = f"File: {file}, belongs to Peers: {peers}\r\n"
        msg = bytes(string_to_send, 'utf-8')
        client.send(msg)
    client.send(b'\r\n')

def menu(client):
    """Helper function that displays user-friendly menu to each client."""
    while True:
        msg1 = b'Welcome to the indexing server! What would you like to do?\r\n'
        msg2 = b'----------------------------------------------------------\r\n'
        msg3 = b'1: Search for a file and download it\r\n'
        msg4 = b'2: See the list of files and the Peer(s) they belong to\r\n'
        msg5 = b'\r\n'
        msg6 = b'Choice: '
        client.send(msg1 + msg2 + msg3 + msg4 + msg2 + msg6)
        choice_string = client.recv(8)
        try:
            choice = int(choice_string)
            if choice > 2:
                client.send(msg5)
                err_msg = b'Please enter in a valid menu choice!\r\n'
                client.send(err_msg)
                client.send(msg5)
                continue
        except ValueError:
            client.send(msg5)
            err_msg = b'Please enter in a valid integer.\r\n'
            client.send(err_msg)
            client.send(msg5)
            continue

        client.send(msg5)
        return choice

# This thread starts the RPC server of the indexing server and runs it in the background
threading.Thread(target=start_remote_methods, daemon=True).start()

# The main thread handles socket connections of the index server
server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server_socket.bind(('', socket_port))
server_socket.listen(5)

# Infinite loop that waits for client's connections, and starts a new thread for each
# client that successfully connects to the server.
# If user does Ctrl-C, the server will exit.
while True:
    try:
        (client, address) = server_socket.accept()
        peers.append(address[0])
        threading.Thread(target=client_handler,
                         args=(client, address,)).start()
    except KeyboardInterrupt:
        print("Exiting...")
        sys.exit(0)

server_socket.close()
