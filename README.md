# File Sharing Peer-to-Peer system

File sharing system using a Peer-to-Peer network architecture, developed in Python.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

gcc/clang, telnet

### Installing
#### Ubuntu
The Makefile includes the commands necessary to install everything you need on Ubuntu, just run:
```
make prepare-dev-ubuntu
```
#### Fedora
The Makefile includes the commands necessary to install everything you need on Fedora, just run:
```
make prepare-dev-fedora
```

## Running the network
To run the server, simply run the make command:
```
make
```

To create Peers 1,2, and 3, open a new terminal window for each peer and run:
```
make peer1
```
and
```
make peer2
```
and
```
make peer3
```

The server runs on localhost port 12345, to connect to the server from a client you can do:
```
telnet localhost 12345
```

## Running the tests

## Built With

* [Python](https://www.python.org/) - Programming language used
* [xmlrpc](https://docs.python.org/3/library/xmlrpc.html) - RPC framework
* [socket](https://docs.python.org/3/library/socket.html) - Python's socket library

## Authors

* **Byron Torres** - [Napster P2P](https://gitlab.com/btorres3/file_sharing_p2p)

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details

## Acknowledgments

* My CS 550 course