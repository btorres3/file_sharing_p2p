from setuptools import setup

setup(
   name='p2p',
   version='1.0',
   description='Napster-like Peer-to-Peer system',
   author='Byron Torres',
   author_email='btorres3@hawk.iit.edu',
   packages=['p2p'],  #same as name
   install_requires=[], #external packages as dependencies
)