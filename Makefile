PYTHON := python3.7
SRC    := ./p2p
PEER   := ${SRC}/peer.py

.DEFAULT: server

server:
	${PYTHON} ${SRC}/server.py
peer1:
	${PYTHON} ${PEER} 1 12346 9091
peer2:
	${PYTHON} ${PEER} 2 12347 9092
peer3:
	${PYTHON} ${PEER} 3 12348 9093

prepare-dev-ubuntu:
	@sudo apt-get -y install telnetd python3.7 python3-pip

prepare-dev-fedora:
	@sudo dnf install python3.7 python3-pip